package com.epam.dto;

import com.epam.model.Batch;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@Builder
public class AssociateDto {
	private int id;
	@NotBlank(message = "Associate Name should be a Valid")
	private String name;
	@Email(message = "Email should a Valid Format")
	private String email;
	@NotBlank(message = "Gender should be a valid type")
	@Pattern(regexp = "M|F")
	private String gender;
	@Pattern(regexp = "ACTIVE|INACTIVE")
	@NotBlank(message = "Gender should be a valid")
	private String status;
	private Batch batch;
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public String getGender() {
		return gender;
	}
	public String getStatus() {
		return status;
	}
	public Batch getBatch() {
		return batch;
	}
	public void setBatch(Batch batch) {
		this.batch = batch;
	}
	
	
}
