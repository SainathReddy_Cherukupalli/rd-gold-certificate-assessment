package com.epam.dto;

import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
public class BatchDto {
	private int id;
	@Column(unique = true)
	@NotBlank(message = "Batch name should be valid")
	private String name;
	@Pattern(regexp = "JAVA|PYTHON|TESTING|DEP|DOT NET|FRONT END")
	private String practice;
	@NotBlank(message = "StartDate should be valid format ")
	private Date startDate;
	@NotBlank(message = "Batch name should be valid")
	private Date endDate;
	
	List<AssociateDto> associates;
}
