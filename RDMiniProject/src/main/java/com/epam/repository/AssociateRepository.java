package com.epam.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.model.Associate;

@Repository
public interface AssociateRepository extends JpaRepository<Associate, Integer> {
	List<Associate> findByGender(String gender);
	
	Optional<Associate> findByEmail(String email);

}
