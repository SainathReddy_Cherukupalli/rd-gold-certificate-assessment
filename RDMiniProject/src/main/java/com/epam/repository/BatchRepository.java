package com.epam.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.model.Batch;

@Repository
public interface BatchRepository extends JpaRepository<Batch, Integer> {
	boolean existsByName(String name);
	Optional<Batch> findByName(String name);

}
