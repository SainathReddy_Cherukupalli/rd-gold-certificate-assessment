package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociateDto;
import com.epam.service.AssociateService;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestController
@RequestMapping("rd/associates")
public class AssociateController {
	
	@Autowired
	AssociateService associateService;
	
	@PostMapping
	public ResponseEntity<AssociateDto> create(@RequestBody	AssociateDto associateDto){
		log.info("Associate create initiated");
		return new ResponseEntity<>(associateService.add(associateDto),HttpStatus.CREATED);
	}
	
	@GetMapping("{gender}")
	public ResponseEntity<List<AssociateDto>> get(@PathVariable String gender){
		log.info("Retrieving List associates by gender");
		return ResponseEntity.ok().body(associateService.getAssociates(gender));
	}
	
	@PutMapping
	public ResponseEntity<AssociateDto> update(@RequestBody AssociateDto associateDto){
		log.info("Updating Associate");
		return ResponseEntity.ok().body(associateService.update(associateDto));
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> deleteAssociate(@PathVariable("id") int id){
		log.info("Deleting associate");
		associateService.delete(id);
		return ResponseEntity.noContent().build();
	}
}
