package com.epam.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dto.AssociateDto;
import com.epam.exception.AssociateException;
import com.epam.model.Associate;
import com.epam.model.Batch;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class AssociateServiceImpl implements AssociateService {

	@Autowired
	AssociateRepository associateRepo;

	@Autowired
	BatchRepository batchRepo;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public AssociateDto add(AssociateDto associateDto) {
		log.info("Accosiation Creation");
		Associate associate = modelMapper.map(associateDto, Associate.class);
		Optional<Batch> batch = batchRepo.findByName(associateDto.getBatch().getName());
		if (batch.isPresent()) {
			associate.setBatch(batch.get());
			batchRepo.save(batch.get());
		} else {
			batchRepo.save(associateDto.getBatch());
		}
		associateRepo.save(associate);
		return modelMapper.map(associate, AssociateDto.class);
	}

	@Override
	public List<AssociateDto> getAssociates(String gender) {
		log.info("Retieved Associates by gender");
		return associateRepo.findByGender(gender).stream()
				.map(associate -> modelMapper.map(associate, AssociateDto.class)).toList();
	}

	@Override
	public AssociateDto update(AssociateDto associateDto) {
		log.info("Updating Associate");
		Associate associate = associateRepo.findByEmail(associateDto.getEmail())
				.orElseThrow(() -> new AssociateException("Associate not found"));

		associateRepo.save(Associate.builder().id(associate.getId()).email(associate.getEmail())
				.name(associateDto.getName()).gender(associateDto.getGender()).status(associateDto.getStatus())
				.batch(associate.getBatch()).build());
		return modelMapper.map(associate, AssociateDto.class);
	}

	@Override
	public void delete(int id) {
		log.info("Deleting associate");
		Associate associate = associateRepo.findById(id)
				.orElseThrow(() -> new AssociateException("Associate not found"));
		associate.setBatch(null);
		associateRepo.deleteById(id);
	}

}
