package com.epam.service;

import java.util.List;

import com.epam.dto.AssociateDto;

public interface AssociateService {
	AssociateDto add(AssociateDto associateDto);
	
	List<AssociateDto> getAssociates(String gender);
	
	AssociateDto update(AssociateDto associateDto);
	
	void delete(int id);
}
