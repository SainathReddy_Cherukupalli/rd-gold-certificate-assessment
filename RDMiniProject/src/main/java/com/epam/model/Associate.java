package com.epam.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity

@AllArgsConstructor
@Builder
public class Associate {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String email;
	private String gender;
	private String status;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn
	private Batch batch;
	public void setBatch(Batch batch) {
		this.batch = batch;
	}
	public Batch getBatch() {
		return batch;
	}
	public int getId() {
		return id;
	}
	public String getEmail() {
		return email;
	}
	
	
}
