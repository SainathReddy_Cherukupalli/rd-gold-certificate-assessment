package com.epam.exception;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ExceptionResponse> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			WebRequest req) {
		List<String> inputErrors = new ArrayList<>();
		ex.getAllErrors().forEach(err -> {
			inputErrors.add(err.getDefaultMessage());
		});
		log.error("MethodArgumentNotValidException:{}", inputErrors.toString());
		ExceptionResponse error = new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(),
				inputErrors.toString(), req.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(AssociateException.class)
	public ResponseEntity<ExceptionResponse> handleException(Exception ex, WebRequest req) {
		log.error("{}: {}", ex.getClass().getSimpleName(), ExceptionUtils.getStackTrace(ex));

		ExceptionResponse error = new ExceptionResponse(new Date().toString(), HttpStatus.OK.toString(),
				ex.getLocalizedMessage(), req.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.OK);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<ExceptionResponse> handleMethodArgumentTypeMismatchException(RuntimeException ex,
			WebRequest req) {
		log.error("MethodArgumentTypeMismatchException :{}", ExceptionUtils.getStackTrace(ex));
		ExceptionResponse error = new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(),
				ex.getLocalizedMessage(), req.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ExceptionResponse> handleRuntimeException(RuntimeException ex, WebRequest req) {
		log.error("RuntimeException :{}", ExceptionUtils.getStackTrace(ex));
		ExceptionResponse error = new ExceptionResponse(new Date().toString(),
				HttpStatus.INTERNAL_SERVER_ERROR.toString(), ex.getLocalizedMessage(), req.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
