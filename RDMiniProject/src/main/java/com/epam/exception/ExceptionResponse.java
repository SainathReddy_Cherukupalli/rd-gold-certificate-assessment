package com.epam.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@AllArgsConstructor
public class ExceptionResponse {
	String timestamp;
	String status;
	String error;
	String path;
}
 