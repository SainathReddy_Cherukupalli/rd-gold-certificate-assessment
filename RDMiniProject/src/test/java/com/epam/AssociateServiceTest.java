package com.epam;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dto.AssociateDto;
import com.epam.dto.BatchDto;
import com.epam.exception.AssociateException;
import com.epam.model.Associate;
import com.epam.model.Batch;
import com.epam.repository.AssociateRepository;
import com.epam.repository.BatchRepository;
import com.epam.service.AssociateServiceImpl;

@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {

	@Mock
	private AssociateRepository associateRepo;

	@Mock
	private BatchRepository batchRepo;

	@Mock
	ModelMapper modelMapper;

	@InjectMocks
	private AssociateServiceImpl associateService;

	private Associate associate;
	private AssociateDto associateDto;
	private Batch batch;
	private BatchDto batchDto;

	@BeforeEach
	public void setUp() {
		batch = Batch.builder().id(1).name("test").practice("JAVA").associates(List.of()).endDate(new Date()).startDate(new Date()).build();
		batchDto = BatchDto.builder().id(1).name("test").practice("JAVA").associates(List.of()).endDate(new Date()).startDate(new Date()).build();
		associate = Associate.builder().id(1).gender("M").name("test").email("test@gmail.com").status("INACTIVE").build();
		associateDto = AssociateDto.builder().id(1).gender("M").name("test").email("test@gmail.com").status("INACTIVE").build();
	}

	@Test
	void getAssociatesByGender() {
		String gender = "M";
		Mockito.when(associateRepo.findByGender(gender)).thenReturn(List.of(associate));
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		List<AssociateDto> retrieved = associateService.getAssociates(gender);
		Mockito.verify(associateRepo).findByGender(gender);
	}

	@Test
	void testDeleteAssociate() {
		int id = 1;
		Mockito.when(associateRepo.findById(id)).thenReturn(Optional.of(associate));
		Mockito.doNothing().when(associateRepo).deleteById(id);
		associateService.delete(id);
		Mockito.verify(associateRepo).deleteById(id);
	}

	@Test
	void testAddAssociateWithExistingBatch() {
		associateDto.setBatch(batch);

		when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
		when(batchRepo.findByName(associateDto.getBatch().getName())).thenReturn(Optional.of(batch));

		associateService.add(associateDto);
		verify(batchRepo, times(1)).findByName(associateDto.getBatch().getName());
		verify(batchRepo, times(1)).save(batch);
		verify(associateRepo, times(1)).save(associate);
		verify(modelMapper, times(1)).map(associate, AssociateDto.class);
	}

	@Test
	void testAddAssociateWithNewBatch() {
		associateDto.setBatch(batch);
		when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
		when(batchRepo.findByName(associateDto.getBatch().getName())).thenReturn(Optional.empty());

		associateService.add(associateDto);
		verify(batchRepo, times(1)).findByName(associateDto.getBatch().getName());
		verify(batchRepo, times(1)).save(any(Batch.class));
		verify(associateRepo, times(1)).save(associate);
		verify(modelMapper, times(1)).map(associate, AssociateDto.class);
	}

	@Test
	void testUpdateAssociate() {

		Mockito.when(associateRepo.findByEmail("test@gmail.com")).thenReturn(Optional.of(associate));
		when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		associateService.update(associateDto);
		Mockito.verify(associateRepo).findByEmail("test@gmail.com");
	}
	
	@Test
	void testDeleteAssociateException() {
		Mockito.when(associateRepo.findById(100)).thenReturn(Optional.empty());
		assertThrows(AssociateException.class, () -> associateService.delete(100));
		Mockito.verify(associateRepo).findById(100);
	}
	@Test
	void testUpdateAssociateException() {
		Mockito.when(associateRepo.findByEmail("test@gmail.com")).thenReturn(Optional.empty());
		assertThrows(AssociateException.class, () -> associateService.update(associateDto));
		Mockito.verify(associateRepo).findByEmail("test@gmail.com");
	}
}
