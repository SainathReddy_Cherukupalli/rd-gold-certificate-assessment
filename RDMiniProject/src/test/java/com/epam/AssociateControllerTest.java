package com.epam;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.AssociateController;
import com.epam.dto.AssociateDto;
import com.epam.model.Associate;
import com.epam.model.Batch;
import com.epam.service.AssociateServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateController.class)
class AssociateControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	AssociateServiceImpl associateService;
	
	private Associate associate;
	private AssociateDto associateDto;
	private Batch batch;
	@BeforeEach
	public void setUp() {
		associate = Associate.builder().id(1).gender("M").name("test").email("test@gmail.com").status("INACTIVE").build();
		associateDto = AssociateDto.builder().id(1).gender("M").name("test").email("test@gmail.com").status("INACTIVE").build();
		batch = Batch.builder().id(1).name("RD").practice("JAVA").startDate(new Date()).endDate(new Date()).build();
	}
	
	@Test
	void testAssociatesById() throws Exception{
		String gender = "M";
		List<AssociateDto> list = Arrays.asList(associateDto); 
		Mockito.when(associateService.getAssociates(gender)).thenReturn(list);
		mockMvc.perform(get("/rd/associates/{gender}","M")).andExpect(status().isOk());
	}
	
	@Test
	void testDeleteAssociate() throws Exception{
		Mockito.doNothing().when(associateService).delete(anyInt());
		mockMvc.perform(delete("/rd/associates/{id}","1")).andExpect(status().isNoContent());
		
	}
	
	@Test
	void testCreateBook() throws Exception{
		Mockito.when(associateService.add(associateDto)).thenReturn(associateDto);
		ObjectMapper map = new ObjectMapper();
		String jsonForm = map.writeValueAsString(associate);
		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
			.andExpect(status().isCreated());
	}
	@Test
	void testUpdateAssociate() throws Exception{
		Mockito.when(associateService.update(associateDto)).thenReturn(associateDto);
		ObjectMapper map = new ObjectMapper();
		String jsonForm = map.writeValueAsString(associate);
		mockMvc.perform(put("/rd/associates")
				.contentType(MediaType.APPLICATION_JSON).content(jsonForm))
		.andExpect(status().isOk());
	}
}
